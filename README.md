# triennale

Contenitore per la stesura della relazione del lavoro svolto durante lo stage.

## Struttura del progetto

```
preamble/
├─ preamble.tex
├─ shorcuts.tex
├─ packages/
│  ├─ packages.tex
│  ├─ minted.tex
│  ├─ tikz.tex
│  ├─ ams.tex
│  ├─ biblatex.tex
├─ my-macros/
│  ├─ my-macros.tex
│  ├─ arabiczeropad.tex
│  ├─ datafile.tex
│  ├─ cutimage.tex
│  ├─ file.tex
│  ├─ inputpath.tex
│  ├─ mintedpath.tex
│  ├─ newthread.tex
│  ├─ prog.tex
├─ workarounds/
│  ├─ workarounds.tex
│  ├─ fixpdfx.tex
│  ├─ issue-1041.tex
```

## Composizione

Alcune buone pratiche da seguire durante la composizione.

### Dimensionamento immagini

Nell'`\includegraphics[width=\linewidth]{...}` usare come fattore di `width` un [valore](https://www.wolframalpha.com/input/?i=Union%5BTable%5BRound%5B0.35*%28sqrt%282%29%29%5En%2C0.1%5D%2C+%7Bn%2C1%2C15%7D%5D%2CTable%5B0.35%2C%7B1%7D%5D%5D) 
della sequenza geometrica di ragione
<img src="https://bit.ly/2h8X02B" align="center" border="0" alt=" \sqrt{2} " width="31" height="26" />.

## Presentazione

Si veda il file `ferron_lorenzo.pptx`. Si fa notare che per modificare il layout della slide bisogna usare lo 
**Schema Diapositiva** (Visualizza > Schema Diapositiva oppure View > Slide Master).

Qualora si abbia bisogno di esportare in PDF per conservare le transizioni basta utilizzare [PPspliT](https://github.com/maxonthegit/PPspliT).
