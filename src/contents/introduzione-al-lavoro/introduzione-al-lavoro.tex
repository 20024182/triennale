% !TeX encoding = UTF-8
% !TeX root = ../../main.tex

\chapter{Introduzione al lavoro}
\label{ch:introduzione-al-lavoro}
\graphicspath{{contents/introduzione-al-lavoro/assets/img/}}

Abbiamo già detto che lo scopo del lavoro è stato quello di realizzare una piattaforma per l'\ecommerce.
Questo termine, entrato nell'uso quotidiano, rende l'idea di ciò che si voleva ottenere, ma non rende giustizia al lavoro svolto, anzi lo minimizza.
Per questo motivo, prima di trattare gli aspetti concreti del lavoro, sarà necessaria una panoramica riguardo i requisiti funzionali e non desiderati.

In particolare nel corso del capitolo, oltre ai requisiti, si prenderanno in esame le motivazioni che hanno portato alla dismissione del precedente sistema informatico, la metodologia utilizzata per lo sviluppo del software e le modalità di lavoro.

\section{Il software precedente}
\label{sec:il-software-precedente}

All'inizio il sistema informatico era fruibile attraverso il software \prog{Access}~\cite{msaccess} con cui era stato realizzato.
Per esigenze puramente aziendali esso non era esposto all'esterno dell'\textenglish{intranet}.
Questa situazione è perdurata fino a quando grandi aziende del commercio elettronico, come \mbox{Amazon} ed \mbox{eBay}, non sono diventate competitor delle più piccole; urgeva quindi la necessità di mettere a disposizione una piattaforma indipendente e dedicata, fu scelto \prog{\mbox{Joomla}}~\cite{joomla}.
Negli stessi anni prendeva anche piede una tipologia di \textenglish{cloud computing}, il \textenglish{Software as Service} (SaaS), che delega l'installazione e la manutenzione del server e dei servizi a terzi, lasciando all'utente la sola configurazione dell'applicazione pagata; si veda anche \cite{Sommerville2017}.
Il difficile uso dell'applicazione portò a un nuovo cambiamento e venne acquistato un nuovo servizio, sempre con le stesse modalità.
Si trattava di \prog{\mbox{WordPress}}~\cite{wordpress}.

\prog{\mbox{WordPress}} non nasce per siti dedicati all'\ecommerce, ma per blog.
Tuttavia, essendo \textenglish{open source} è possibile creare o installare \textenglish{plugin} per adattarlo a qualsiasi esigenza.
L'uso dei \textenglish{plugin} sembra offrire all'utente, anche alle prime armi, un buon punto di partenza per sviluppare la propria piattaforma, ma questo meccanismo nasconde dei difetti.
In primo luogo, ogni \textenglish{plugin} è indipendente dagli altri, il che porterebbe a pensare all'assenza di problemi inerenti la compatibilità, ma non è così, infatti spesso l'incompatibilità può portare a un peggioramento dell'usabilità della piattaforma da parte dei clienti e del gestore.
In secondo luogo, molti \textenglish{plugin} presentano maschere in lingua inglese e, soprattutto per quelli meno conosciuti, non esiste una traduzione oppure è incompleta; ancora una volta i clienti e il gestore, che non padroneggiano la lingua, si trovano in difficoltà.
Inoltre, le poche conoscenze riguardo la sicurezza informatica potrebbero esporre l'applicativo a maggiori vulnerabilità, ad esempio rendendo visibile verso l'esterno il contenuto della cartella \file{wp-content/plugins} è possibile conoscere i \textenglish{plugin} installati.
Pur avendo evitato questo problema, spesso non si effettuano gli aggiornamenti dei \textenglish{plugin}, i quali possono ancora essere scoperti da \textenglish{software} come \prog{\mbox{wpscan}}~\cite{wpscan}, che permette anche di conoscere le vulnerabilità della versione corrente, se non aggiornata, e di fatto aiutare l'attaccante nel suo intento.
Infine l'uso di \textenglish{plugin} per modellare la base di dati, attraverso la definizione di metadati, può risultare difficile al gestore che si trova a dover completare differenti campi di cui a volte non conosce il significato; questo fatto si verifica di frequente qualora il \textenglish{plugin} abbia una scarsa o inesistente documentazione, ma anche quando questa è in inglese.
Non è altresì raro ottenere modelli non sufficientemente aderenti alla realtà, cioè troppo permissivi o grossolani.

Alcuni di questi aspetti potevano essere risolti mettendo mano al \textenglish{file system} su cui risiede l'installazione di \prog{\mbox{WordPress}}.
Abbiamo però accennato al fatto che il servizio è stato acquistato con tipologia SaaS, pertanto non è permesso interagire direttamente con il sistema operativo.
Altri interventi richiedevano una maggiore comprensione dei \textenglish{plugin}, ma non si garantiva il risultato desiderato a causa delle incompatibilità.

Quanto finora detto può far pensare a una situazione disastrosa in cui nulla è salvabile.
In realtà l'impostazione delle pagine e l'organizzazione logica dei prodotti sono state mantenute e migliorate perché ben fatte, ma rimaneva lo scarso risultato ottenuto dall'applicativo in sé, così come per i suoi predecessori.
Pertanto si avevano due scelte: optare per un \textenglish{commercial off-the-shelf} (COTS) oppure un prodotto \textit{ad hoc}.
La scelta è ricaduta su quest'ultimo.

Il perché è presto detto: comodità.
La necessità era uno strumento funzionale al suo scopo, anche se significava meno completo rispetto a quelli commerciali, come \prog{Magento}~\cite{adobemagento}, i quali tuttavia richiedono da parte del committente una maggiore comprensione delle loro funzionalità, al fine di scegliere e utilizzare quelle veramente utili.

\section{La metodologia Scrum}
\label{sec:la-metodologia-scrum}

Il processo di sviluppo del \textenglish{software} è stato affrontato da solo, pertanto si è adottato un modello di sviluppo agile.
La scelta verteva su \textenglish{eXtreme Programming} (XP) o Scrum, ma ricadde su quest'ultimo.
Le motivazioni riguardavano l'assenza della programmazione a coppie e lo sviluppo con test iniziali.

Il punto di partenza del processo Scrum sono le caratteristiche del prodotto e i suoi requisiti, che vanno sotto il nome di \textenglish{product backlog}.
Nella versione iniziale la lista di questi elementi può essere derivata da storie utente o da un'altra descrizione del software.
L'uso delle storie utente si è dimostrato particolarmente efficace -- le persone trovano più semplice relazionarsi con uno scenario d'uso in cui potrebbe trovarsi un utente del sistema, anziché con un tradizionale documento di requisiti o con diagrammi dei casi d'uso.
Inoltre possono essere utili per facilitare la comunicazione tra sviluppatore e committente, coinvolgendo maggiormente quest'ultimo nella fase di deduzione dei requisiti.
Una volta che il committente avrà indicato l'ordine di priorità per le storie, potrà essere creato il primo \textenglish{sprint backlog} selezionando gli elementi dal \textenglish{product backlog} e completandolo entro uno \textenglish{sprint}.
Per esigenze di tempo, ma anche per mantenere aggiornato il committente, si è fissata la durata dello \textenglish{sprint} a una settimana, al termine della quale avvenivano incontri ``a distanza'' dove si mostrava il lavoro fin lì svolto.
Questi momenti di incontro erano anche occasione per revisionare il \textenglish{product backlog} ed eventualmente cambiare la priorità delle storie utente o variare i requisiti.
In \Cref{fig:scrum} è illustrato il processo Scrum, per approfondimenti si veda \cite{Sommerville2017}.
\begin{figure}[t]
	\centering
	\includegraphics[width=.7\linewidth]{scrum.png}
	\caption[Il ciclo degli sprint di Scrum]{Il ciclo degli sprint di Scrum (tratto da \cite{Sommerville2017}).}
	\label{fig:scrum}
\end{figure}

\subsection{I requisiti}
\label{subsec:i-requisiti}

Concludiamo il capitolo dedicando questo \namecref{subsec:i-requisiti} ai requisiti, che saranno ripresi in dettaglio nel \Cref{ch:lavoro-svolto} per analizzarne le eventuali problematiche e le idee implementative.
Si ricorda che i requisiti sono stati raccolti attraverso le storie utente proposte al committente, come prevede la metodologia Scrum.

Cominciamo dai requisiti funzionali espressi, per semplicità, come requisiti dell'utente, cioè con un linguaggio naturale e un alto livello di astrazione.
\begin{enumerate}
[%
	label=RF\arabiczeropad{*}{2},
	format=\bfseries\upshape\sffamily
]
	\item\label{itm:product} Ogni prodotto deve avere uno \textenglish{\textit{Stock Keep Unit}} (SKU): un identificativo univoco all'interno del magazzino.
	Inoltre deve avere un nome, un'immagine, una descrizione, un prezzo base da listino e una categoria di appartenenza, ad esempio ``tavolo in acciaio'' o ``sedia in acciaio''.

	\item Ogni prodotto deve mostrare una media delle proprie valutazioni complessive.

	\item L'utente deve essere avvertito qualora il prodotto sia recentemente stato aggiunto.

	\item\label{itm:customization} Il prodotto può essere personalizzato a piacere dal cliente, la personalizzazione ne determina l'aumento o meno del prezzo.
	Anche in questo caso deve essere possibile una visualizzazione del prodotto personalizzato che si vorrà acquistare.

	\item\label{itm:part} Per ogni prodotto, se disponibile, deve essere acquistabile ogni suo ricambio.

	\item\label{itm:part-isa-product} Un ricambio è un prodotto.

	\item\label{itm:category} Ogni categoria deve poter contenere nessuna, una o più sottocategorie indipendentemente dalla presenza di prodotti al suo interno.

	\item Le categorie devono avere un nome e deve essere possibile indicare all'utente quelle create di recente.

	\item\label{itm:user-type} Il cliente può essere sia un'azienda sia un privato.

	\item\label{itm:sign-up} La registrazione di un nuovo cliente richiede \textbf{solamente} un'\textenglish{e-mail} e una password.
	Al termine della procedura l'\account deve essere attivato dall'apposito link inviato all'\textenglish{e-mail} del nuovo \textbf{potenziale} acquirente.

	\item\label{itm:privileges} Qualora l'\account non venga attivato l'utente, pure essendo registrato, non avrà alcun privilegio rispetto ad uno non registrato.

	\item Il cliente deve poter, in un secondo momento, completare il proprio profilo.

	\item Il cliente durante il completamente del suo profilo deve registrare almeno un indirizzo e un numero di telefono, da poter fornire al corriere.
	Tutti i dati del profilo, compresa l'\textenglish{e-mail}, devono poter essere modificabili.

	\item Il \textenglish{form} di completamento del profilo di un privato richiede nome, cognome e codice fiscale, per poter intestare la fattura.
	Inoltre dal codice fiscale è possibile estrarre la data di nascita, eventualmente utile per promozioni mirate basate sul compleanno.

	\item L'azienda è da considerasi come un utente, l'\textenglish{e-mail} che fornirà potrebbe anche essere quella del responsabile degli acquisti.
	In ogni caso anche per l'azienda deve essere previsto un \textenglish{form} apposito, che richieda la partita IVA, necessaria per intestare le fatture e il nome dell'azienda.
	Inoltre deve essere presente almeno uno tra PEC e codice univoco, utili per la fatturazione elettronica.

	\item\label{itm:login} L'accesso all'\account utente deve poter essere effettuabile attraverso l'\textenglish{e-mail} e la password fornite in fase di registrazione.

	\item Il prodotto deve poter essere visibile anche prima di entrare di fatto in commercio, in tal modo l'utente interessato, registrato o meno, potrà fare richiesta di notifica via \textenglish{e-mail}, che sarà inviata non appena il prodotto diventerà acquistabile.
	Un discorso analogo vale per i prodotti esauriti, ma non ritirati.

	\item I prodotti ritirati devono essere consultabili, per poter reperire se necessario ricambi, ma non sarà possibile acquistarli né vederne il prezzo, tuttavia vi si potrà lasciare una recensione.
	Questo perché un cliente potrebbe voler comunque esprimere il suo apprezzamento.

	\item\label{itm:substitutes} Ogni prodotto ritirato deve poter indicare quali sono i suoi sostituti, se presenti.

	\item L'utente, registrato o non, potrà effettuare ricerche o sfogliare il catalogo dei prodotti.
	In entrambi i casi verrà mostrato il prezzo base da listino e sarà possibile ordinare i prodotti per prezzo o per nome.

	\item Al cliente deve essere mostrato l'imponibile tassato alla selezione di un articolo o alla visualizzazione del carrello.

	\item Il cliente deve essere informato qualora la quantità del prodotto scenda sotto un numero prefissato di unità.

	\item L'utente registrato e non deve poter modificare il proprio carrello.

	\item Il carrello di un utente loggato deve poter essere recuperato, ed eventualmente modificato, al successivo \textenglish{login}, che potrebbe avvenire a distanza di anni.

	\item Qualora nel carrello recuperato vi fossero articoli ritirati o esauriti, l'utente deve essere avvertito e il totale deve essere aggiornato di conseguenza, ovvero senza tenere conto di questi articoli.

	\item\label{itm:profile} Prima di procedere all'acquisto, è logico che il profilo utente risulti completo in ogni sua parte.

	\item\label{itm:payment} L'acquisto di un prodotto avviene per mezzo di carta di credito oppure PayPal.

	\item\label{itm:search} La ricerca nel catalogo può includere termini contenuti sia nel nome del prodotto sia nelle sua descrizione.

	\item Qualsiasi utente, registrato o non, può visualizzare le recensioni lasciate da altri.
	In particolare deve poter ordinarle per ``più recenti'' oppure per gradimento crescente o decrescente.

	\item L'utente può recensire un prodotto da lui non acquistato.

	\item L'utente può recensire il prodotto non più di una volta.

	\item L'inserimento di una recensione richiede, qualora non sia già stato fatto, il completamento dell'\account.

	\item\label{itm:review} Nel completare il \textenglish{form} per la recensione l'utente potrà specificare opzionalmente un titolo, una valutazione complessiva e, ovviamente, la recensione stessa.

\end{enumerate}

Infine, di seguito sono riportati i requisiti non funzionali.

\begin{enumerate}
[%
label=RNF\arabiczeropad{*}{2},
format=\bfseries\upshape\sffamily
]

\item L'interfaccia utente sarà grafica.
\item Il codice sarà scritto in Java e si eseguirà su \mbox{\prog{Tomcat}}.
\item Il database utilizzato sarà MySQL\@.
\item Lo sviluppo inizia a fine giugno 2020 e la consegna è prevista per metà agosto 2020.
\item\label{itm:pci-dss} Rispettare lo standard PCI~DSS\@.
\item Rispettare il GDPR\@.
\item Le date saranno memorizzate nel formato AAAA/MM/GG\@.
\item\label{itm:utf-8} Gli input saranno memorizzati nella codifica UTF-8.

\end{enumerate}
