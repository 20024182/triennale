package com.trimaral.dao;

import com.google.i18n.phonenumbers.NumberParseException;
import com.trimaral.entity.Login;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author Lorenzo Ferron
 * @version 2019.09.05
 */
public class LoginDAO extends DAO<Login> {

    public LoginDAO(DataSource dataSource) {
        super(dataSource, Login.class);
    }

    public Login findByEmail(String email) throws SQLException, NumberParseException {
        return findOneByCriteria("email = " + getQuotedString(email));
    }
}
