package com.trimaral.servlet.customer;

import com.trimaral.Roles;
import com.trimaral.dao.LoginDAO;
import com.trimaral.dao.UserRoleDAO;
import com.trimaral.entity.Login;
import com.trimaral.entity.UserRole;
import com.trimaral.utils.PasswordCreator;
import com.trimaral.utils.SMTPMXLookup;
import com.trimaral.utils.VerifyRecaptcha;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet(name = "Signup", urlPatterns = {"%2Fsignup"})
public class Signup extends HttpServlet {

    @Resource(name = "jdbc/TrimarDB")
    private DataSource dataSource;
    private LoginDAO loginDAO;

    @Override
    public void init() throws ServletException {
        loginDAO = new LoginDAO(dataSource);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String email = request.getParameter("email");
            Login login = loginDAO.findByEmail(email);
            if (login != null) {
                request.getRequestDispatcher("/WEB-INF/pages/customer/signup.jsp?exists=true").forward(request, response);
                return;
            }
            String newPassword = request.getParameter("newPassword");
            String confirmPassword = request.getParameter("confirmPassword");
            if (newPassword.length() < Login.PASSWORD_LENGTH)
                throw new IllegalArgumentException("Password too short");
            if (!newPassword.equals(confirmPassword))
                throw new IllegalArgumentException("Passwords don't match");
            if (request.getParameter("termsCheckbox") == null)
                throw new IllegalArgumentException("Unchecked Terms & Conditions");

            // get reCAPTCHA request param
            String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
            boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
            if (!verify) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }

            if (!SMTPMXLookup.isAddressValid(email)) {
                request.getRequestDispatcher("/WEB-INF/pages/customer/signup.jsp?invalid=true").forward(request, response);
                return;
            }

            login = new Login();
            login.setEmail(email);
            login.setPassword(PasswordCreator.getHexPassword(newPassword));
            loginDAO.save(login);
            UserRole userRole = new UserRole();
            userRole.setEmail(login.getEmail());
            userRole.setRoleName(String.valueOf(Roles.PRIVATO));
            UserRoleDAO userRoleDAO = new UserRoleDAO(dataSource);
            userRoleDAO.save(userRole);
            request.getSession().setAttribute("requestEmail", login);
            response.sendRedirect(request.getContextPath() + "/confirm-email");
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/pages/customer/signup.jsp").forward(request, response);
    }
}
