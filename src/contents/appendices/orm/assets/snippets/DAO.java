package com.trimaral.dao;

import com.google.i18n.phonenumbers.NumberParseException;
import com.trimaral.orm.ResultSetMapper;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 * @param <T> a DAO class
 * @author Lorenzo Ferron
 * @version 2019.09.07
 */
public abstract class DAO<T extends Serializable> {

    protected final ResultSetMapper<T> resultSetMapper;
    protected final DataSource dataSource;

    protected DAO(DataSource dataSource, Class<T> clazz) {
        this.dataSource = dataSource;
        this.resultSetMapper = new ResultSetMapper<>(dataSource, clazz);
    }

    public static String getQuotedString(String s) {
        return "'" + s + "'";
    }

    public List<T> findAll() throws SQLException, NumberParseException {
        return findByCriteria(null);
    }

    public List<T> findByCriteria(String criteria, Object... params) throws SQLException, NumberParseException {
        return resultSetMapper.findByCriteria(criteria, params);
    }

    public T findOneByCriteria(String criteria, Object... params) throws SQLException, NumberParseException {
        resultSetMapper.getByCriteria(criteria, params);
        try {
            return findByCriteria(criteria, params).get(0);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public T findById(Long id) throws SQLException, NumberParseException {
        return findOneByCriteria(resultSetMapper.findById(), id);
    }

    public void save(T item) throws SQLException, NumberParseException {
        resultSetMapper.save(item, false, false);
    }

    public void replace(T item) throws SQLException, NumberParseException {
        resultSetMapper.save(item, true, false);
    }

    public void update(T item) throws SQLException, NumberParseException {
        resultSetMapper.update(item);
    }

    public void updateByCriteria(T item, String criteria, Object... params) throws SQLException, NumberParseException {
        resultSetMapper.updateByCriteria(item, criteria, params);
    }

    public void updatesByCriteria(T[] items, String criteria, Object... params) throws SQLException, NumberParseException {
        for (T item : items)
            updateByCriteria(item, criteria, params);
    }

    public void delete(T item) throws SQLException, NumberParseException {
        resultSetMapper.delete(item);
    }

    public void deleteByCriteria(String criteria, Object... params) throws SQLException, NumberParseException {
        resultSetMapper.deleteByCriteria(criteria, params);
    }

    public void deleteById(Long id) throws SQLException, NumberParseException {
        resultSetMapper.deleteById(id);
    }

    public Object customQuery(String query, Object... params) throws SQLException, NumberParseException {
        return resultSetMapper.customQuery(query, params);
    }
}
