package com.trimaral.entity;

import com.google.common.base.Preconditions;
import com.trimaral.orm.annotations.Column;
import com.trimaral.orm.annotations.Table;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Lorenzo Ferron
 * @version 2019.09.05
 */
@Table(name = "login")
public class Login implements Serializable {

    public static final short PASSWORD_LENGTH = 8;

    @Column(name = "password")
    private String password;

    @Column(name = "is_valid", hasDefaultValue = true)
    private Boolean isValid;

    @Column(name = "login_id", isPrimaryKey = true)
    private Long loginId;

    @Column(name = "email")
    private String email;

    @Column(name = "enroll_date", hasDefaultValue = true, hold = true)
    private Timestamp enrollDate;

    private List<EmailConfirmation> confirmations;

    public Login() {
        confirmations = new ArrayList<>(0);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }


    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        Preconditions.checkNotNull(email);
        Preconditions.checkArgument(Pattern.compile("^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])$", Pattern.CASE_INSENSITIVE).matcher(email).matches());
        this.email = email.trim();
    }

    public List<EmailConfirmation> getConfirmations() {
        return confirmations;
    }

    public void setConfirmations(List<EmailConfirmation> confirmations) {
        this.confirmations = confirmations;
    }

    public Timestamp getEnrollDate() {
        return enrollDate;
    }

    public void setEnrollDate(Timestamp enrollDate) {
        this.enrollDate = enrollDate;
    }
}
