% !TeX encoding = UTF-8
% !TeX root = ../../FERRON_relazione.tex

\chapter{Strumenti utilizzati}
\label{ch:strumenti-utilizzati}
\graphicspath{{contents/strumenti-utilizzati/assets/img/}}

In questo capitolo si tratteranno alcuni degli strumenti fin qui citati.
In particolare si dedicherà un paragrafo a \mbox{\prog{Tomcat}} vista la sua centralità nel progetto.
Nel successivo \namecref{sec:tomcat} si raggrupperanno tutti gli altri strumenti usati, ma non si tratteranno le librerie, anche se alcune sono già state citate nel precedente capitolo.

\section{Tomcat}
\label{sec:tomcat}

Nel capitolo precedente, abbiamo detto che \mbox{\prog{Tomcat}}~\cite{tomcat} non è un vero e proprio \textenglish{application server} in Java, come \mbox{\prog{WildFly}}~\cite{wildfly}, ma è un \textenglish{web server} e un \textenglish{servlet container}, come mostrato nella \Cref{fig:architecture}.
Per chiarire meglio questo aspetto è necessario considerare i due componenti separatamente.

\mbox{\prog{Tomcat}} usa un \textenglish{web server} per ricevere richieste e inviare risposte in HTTP(S).
Come abbiamo già visto nel \Cref{sec:architettura}, una volta che la richiesta giunge al \textenglish{web server}, viene passata al \textenglish{servlet container} (o \textenglish{web container}), che ha il compito di soddisfarla.
In particolare, in quest'ultimo componente, \mbox{Tomcat} non implementa tutte le specifiche di JavaEE (o JakartaEE~\cite{Ferrara2019}), ma solo quelle di J2EE ovvero JSP (\mbox{JSR-245}~\cite{Chung2013}), Servlet (\mbox{JSR-369}~\cite{Burns2017}), EL (\mbox{JSR-341}~\cite{Chung2013a}) e WS (\mbox{JSR-356}~\cite{Bucek2014}), manca per esempio il supporto nativo per mappare le tabelle di un \textenglish{database} relazionale in classi Java, perché non è stata implementata la specifica JPA (\mbox{JSR-338}~\cite{DeMichiel2017}), presente invece in \mbox{\prog{WildFly}}.

La \Cref{fig:simplified-req-process}, che illustra il processo di richiesta di una pagina, permette di spiegare sinteticamente\footnote{Per osservare il processo nella sua interezza si faccia riferimento a \cite{request-process}.} alcune delle specifiche implementate in \mbox{\prog{Tomcat}}.
Dopo aver lasciato il \textenglish{web server}, la richiesta può raggiungere un filtro, ovvero un'istanza di un'implementazione concreta dell'interfaccia \texttt{Filter}, la cui specifica è contenuta in \mbox{JSR-369}.
Il compito di questo componente è effettuare operazioni preliminare sulla richiesta oppure sulla futura risposta, ad esempio è possibile configurare l'\textenglish{header} HTTP per quelle pagine che richiedo autenticazione (vedi \Cref{subsec:il-problema-della-cache}).
In questo componente non è previsto l'accesso alla base di dati, anche perché per sua natura un filtro deve essere molto veloce e quindi deve necessariamente fare poche operazioni.

Al termine dell'esecuzione di un filtro è possibile che la richiesta, con la sua risposta, passi a un nuovo filtro e poi ad un altro ancora, in pratica, prima di giungere alla servlet, la richiesta ha attraversato una catena (\textit{chain}) di filtri, l'ultimo dei quali la passerà alla servlet.
\begin{figure}[t]
	\centering
		\begin{sequencediagram}
			\newthread{browser}{:\textenglish{Browser}}
			\newthread[gray!30][1]{web-server}{:\textenglish{Web Server}}
			\newinst[1.75]{filter}{<<\textenglish{Filter}>>}
			\newinst[1]{servlet}{<<Servlet>>}
			\newinst[1]{jspservlet}{<<JspServlet>>}

			\begin{call}{browser}{HTTPS}{web-server}{HTML \textenglish{Page}}
				\begin{call}{web-server}{\textenglish{methodInvocation()}}{filter}{HTML \textenglish{Page}}
					\begin{call}{filter}{\textenglish{\texttt{doFilter()}}}{servlet}{HTML \textenglish{Page}}
						\begin{call}[2]{servlet}{\textenglish{\texttt{forward()}}}{jspservlet}{HTML \textenglish{Page}}
						\end{call}
					\end{call}
				\end{call}
			\end{call}
		\end{sequencediagram}

	\caption{UML \textenglish{sequence diagram} semplificato per una richiesta HTTP.}
	\label{fig:simplified-req-process}
\end{figure}
Una servlet è un programma Java che soddisfa concretamente la richiesta e per fare ciò, oltre a eseguire computazione, potrebbe dover accedere a dati presenti sul \textenglish{database}.
Alla prima richiesta o all'avvio del \textenglish{server} vengono create in memoria le istanze per ogni filtro, servlet o \textenglish{listener}.
In particolare per le servlet viene lanciato il metodo \texttt{init()}, nel quale si sono inizializzate le variabili condivise tra tutte le richieste, infatti al sopraggiungere di una di loro viene risvegliata l'istanza della servlet che è mappata sul corrispondente percorso.
Nelle Java \textenglish{Specification Requests} (JSR), la scelta di risvegliare una servlet è stata fatta per questioni di prestazioni, infatti risulterebbe troppo oneroso dover creare e distruggere (\texttt{destroy()}) una servlet per ogni richiesta.
Per tanto questa implementazione risulta ``\textenglish{thread safe}'' solo quando le variabili sono locali ai metodi.
Nella \Cref{fig:simplified-req-process} l'oggetto \verb|<<Servlet>>| rappresenta un'istanza riesumata, che computa la richiesta.

L'esecuzione della servlet potrebbe non necessitare di dover impaginare i risultati ottenuti, se così non fosse questo compito è lasciato alle JavaServer Page (JSP).
Una JSP impagina il risultato di una computazione attraverso l'HTML, il JavaScript e il CSS, inoltre è possibile inserirvi frammenti di altre pagine JSP (\texttt{.jspf}) o di codice Java (\textenglish{scriptlet}), anche se generalmente quest'ultimo approccio è sconsigliato.
Il motivo è dovuto al fatto che non vi sarebbe una separazione tra il livello di presentazione e quello di \textenglish{business}, tuttavia non si rinuncia completamente alla flessibilità portata da un linguaggio ad alto livello come Java, infatti si usa JSTL (\mbox{JSR-52}~\cite{Delisle2006}).
Questa libreria definisce nuovi tag HTML, come \verb|<c:if>| o \verb|<c:forEach>|, che non saranno visibili lato \textenglish{client}, ma rendono il codice HTML facile da leggere e comprendere.
Inoltre si ha anche la possibilità di crearsi i propri tag, ad esempio se n'è creato uno per mostrare ricorsivamente l'annidamento gerarchico delle categorie.
Dato che è possibile inserire codice Java nella JSP, allora si può scrivere l'intera logica di una servlet all'interno di una di queste pagine.
Risulta quindi che anche una JSP è una servlet, ma che ovviamente deve prima essere trasformata in tale.
A questo punto la pagina è stata creata e può essere spedita all'utente.

Infine si tiene a precisare che la versione di Java usata è stata la 8, anche se erano accettabili le versioni successive.
Questa scelta ha permesso di fare uso degli EL \textenglish{Stream}, ovvero particolari implementazioni degli \textenglish{stream} definiti nella specifica dell'\textenglish{Expression Language} (EL).
Come per gli \textenglish{stream}, introdotti da Java 8, anche gli EL \textenglish{Stream} prendono in input una lambda, permettendo uno stile di programmazione funzionale all'interno dei JSP, che è utile ad evitare \textenglish{scriptlet}, anche quando sono strettamente necessari.

\section{Altri strumenti usati}
\label{sec:altri-strumenti-usati}

In questo \nameCref{sec:altri-strumenti-usati} seguirà una rapida rassegna degli altri strumenti utilizzati nel progetto.
\begin{itemize}
	\item \mbox{\prog{Braintree}}~\cite{braintree} è una piattaforma di pagamento \textenglish{on-line}, che permette di raccogliere i dati del metodo di pagamento per effettuare le transazioni.
	Tra i metodi di pagamento accettati se ne sono scelti due: quello delle carte di credito e quello di PayPal.

	Prima di concludere l'ordine l'utente è tenuto a scegliere quale metodo di pagamento intende usare, in base alla scelta comparirà un opportuno \textenglish{form} da completare.
	Ad esempio per le carte di credito verrà richiesto il PAN, il titolare, il CVV e la data di validità, mentre per PayPal sarà semplicemente \textenglish{e-mail} e \textenglish{password}.
	Si noti che un primo controllo fatto direttamente lato \textenglish{client} sarà quello dell'algoritmo di Luhn, il quale calcola un \textenglish{checksum} sul valore fornito del PAN\@.
	Questo test indica semplicemente se un PAN è valido, ma non garantisce l'esistenza di una carta di credito.

	Una volta che i dati sono stati raccolti sono validati da \mbox{\prog{Braintree}}, il quale decide se la transazione potrà essere completata, infatti le transazioni non sono completate sul momento, ma vengono evase a gruppi.
	Essendo questa un'operazione asincrona, viene messo a disposizione da \mbox{\prog{Braintree}} un meccanismo di \textenglish{webhook}, che permette di far sapere all'\ecommerce quando l'operazione è conclusa.
	Tutto questo se non dovesse sorgere nessun errore, in caso contrario quest'ultimo sarà immediatamente segnalato.
	Questo spiega l'uso, l'opzionalità e la mutua esclusione delle colonne \verb|settled_at| e \verb|error_msg| nella tabella \verb|sale_order| della \Cref{fig:eer} (si veda anche il \Cref{subsec:schema-concettuale}).
	Il pagamento con PayPal richiede invece tempi più brevi di attesa per essere finalizzato.

	\mbox{\prog{Braintree}} permette la creazione di un \account, attraverso cui è possibile monitore tutte le transazioni eseguite e durante la fase di sviluppo mette a disposizione una modalità di \textenglish{\textit{sandbox}} per effettuare le dovute sperimentazioni.

	\item \mbox{\prog{Cloudinary}}~\cite{cloudinary} è un servizio di hosting di file multimediali con una ricca API per manipolare immagini.
	In particolare mette a disposizione tag personalizzati basati sulla libreria JSTL, a cui è possibile passare il nome di un file, presente nella libreria dell'\account, creato sul servizio, per vedere l'immagine corrispondete inserita nella pagina HTML che si sta componendo.
	Molto utile sono le manipolazioni messe a disposizione, una tra tutte il \textenglish{\textit{cropping}}.
	Se n'è fatto uso per conservare l'immagine di una particolare ``configurazione'' del prodotto.

	\item \prog{\mbox{MySQL}}~\cite{mysql} è il DBMS usato per manipolare i dati.
	In particolare si è usata la versione 8, perché questa mette a disposizione il nuovo vincolo \texttt{CHECK}, attraverso cui è possibile effettuare controlli sulla correttezza dei dati prima che vengano scritti sul \textenglish{database}.
	Sono ammessi solo controlli su una componente costante, quindi le colonne che trattano il tempo non possono essere usate come test del vincolo, perché una riga inserita in un dato istante di tempo potrebbe essere invalida in un altro.
	In questi casi l'unico controllo ammissibile è un \textenglish{trigger} sull'inserimento.

	Lo strumento usato per gestirlo è stato \prog{\mbox{MySQL} Workbench}~\cite{OracleCorporation2020}.

	\item \prog{\mbox{VirtualBox}}~\cite{virtualbox} è uno strumento per la virtualizzazione, usato per eseguire il \textenglish{database} su \mbox{Ubuntu} \textenglish{Server}~\cite{ubuntu}, come se fosse su un reale calcolatore fisico.

	\item \mbox{\prog{Front}}~\cite{front} è un tema \textenglish{\textit{multipurpose}} basato su \mbox{\prog{Bootstrap}}~\cite{bootstrap}, che integra varie librerie JavaScript tra cui \mbox{jQuery} \textenglish{Validation}.

\end{itemize}