% !TeX encoding = UTF-8
% !TeX root = ../../../main.tex

\section{Architettura}
\label{sec:architettura}

La scelta dell'architettura è stata fatta sulla base di quattro fattori: il numero di viste giornaliere, i costi per l'\textenglish{hosting} della piattaforma, la staticità dei prodotti dell'azienda e la semplicità.
Considerate queste esigenze, l'architettura più appropriata era quella \textenglish{client-server} a tre livelli, come mostrata in \Cref{fig:architecture}.

Uno dei vantaggi di questa architettura è quello di poter aggiungere controlli su tutti i livelli, avendo la sicurezza di avvisare l'utente appena si verifica un errore senza dover arrivare fino alla base di dati.
In secondo luogo si ha la garanzia che qualora un controllo dovesse mancare su un livello, interviene quello sottostante, non pregiudicando le qualità dell'applicazione (tra cui l'integrità dei dati).
Tuttavia, una tale situazione potrebbe ancora accadere solo se non fosse previsto un dato controllo su nessuno dei livelli.

Dalla \Cref{fig:architecture} salta all'occhio una netta separazione tra la logica dell'applicazione, rappresentata dall'\textenglish{application server}, e la base di dati.
Separando i componenti abbiamo la possibilità di replicarli (\textenglish{scaling out}) e raggrupparli (\textenglish{clustering}) come in \Cref{fig:load-balancing}, per permettere un miglior \textenglish{load balancing}, qualora ci sia un aumento delle visite, e un maggior \textenglish{fault tolerant}.
Inoltre si è ritenuto che tale architettura risultasse maggiormente intuitiva per l'utente rispetto ad una a microservizi, anche se quest'ultima è ormai diventata più diffusa per via dell'alto grado di manutenibilità.
\begin{figure}[t]
	\centering
	\includegraphics[width=.5\linewidth]{load-balancing.pdf}
	\caption{Esempio di \textenglish{scaling out}.}
	\label{fig:load-balancing}
\end{figure}
In particolare permette l'aggiunta di nuove funzionalità o il miglioramento di quelle esistenti, semplicemente sostituendo o espandendo uno dei microservizi.
Tuttavia l'azienda, non essendo di grosse dimensioni, non punta a un continuo rinnovamento, essa preferisce espandere la sua cerchia di clienti, rimando coerente con quelli più fedeli.
Ecco che quindi si presenta un modello molto statico soprattutto nel campionario di prodotti forniti.

Di seguito si illustra brevemente quali sono le interazioni tra \textenglish{client} e \textenglish{server}.
\begin{enumerate}
	\item Il \textenglish{(thin) client}, per esempio un \textenglish{browser}, apre una connessione verso il \textenglish{web server}, al quale invia una richiesta in HTTPS e si mette in attesa (sincronismo).
	\item Il \textenglish{web server} ricevuta la richiesta la passa al \textenglish{web container}.
	Quest'ultimo l'analizza e se necessario recupera i dati dal \textenglish{database}, li manipola e genera dinamicamente la pagina da presentare all'utente.
	\item La richiesta soddisfatta viene nuovamente passata al \textenglish{web server}, che la rinvia al mittente con l'appropriato stato HTTP impostato dal livello precedente per indicare la presenza o meno di errore.
	\item Il \textenglish{client} mostra all'utente la pagina.
	\item Il \textenglish{client} o il \textenglish{server} possono scegliere se terminare la connessione oppure mantenerla aperta per ulteriori richieste.
\end{enumerate}
\begin{figure}[t]
	\centering
	\includegraphics[width=\linewidth]{architecture.pdf}
	\caption{Architettura client-server a tre livelli.}
	\label{fig:architecture}
\end{figure}
Il tipo di architettura appena descritto si adatta molto bene a qualsiasi \textenglish{application server}\footnote{Si è generalizzato un po' troppo con questo termine, infatti \mbox{\prog{Tomcat}} non soddisfa tutti i requisiti di un \textenglish{application server} come si vedrà nel \Cref{ch:strumenti-utilizzati}.} basato su \mbox{JavaEE}, compreso \mbox{\prog{Tomcat}}.
Inoltre è usato anche dalle piattaforme di e-commerce SaaS, come \prog{Magento}.
Tuttavia aziende come \mbox{Amazon} usano un'architettura a microservizi.
Tale scelta comporta avere tanti team quanti i microservizi; ogni team deve essere un \textit{\textenglish{two-pizza team}}, ovvero deve avere un numero di membri che possano essere sfamati con solo due pizze.
Questa filosofia consente un rapido sviluppo, tralasciando lunghe riunioni di progetto, proprio perché il personale è limitato.
\mbox{Amazon}, infatti afferma che può far passare una modifica dalla fase di \textenglish{deployment} a quella di produzione ogni $11,6$ secondi.
Per maggiori dettagli su queste architetture si faccia riferimento a \cite{Richardson2019}.

Nel seguito del capitolo si dedicherà un paragrafo per ciascuno dei livelli presentati in \Cref{fig:architecture}.
