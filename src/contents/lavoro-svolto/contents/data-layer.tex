% !TeX encoding = UTF-8
% !TeX root = ../../../main.tex

\section{Data layer}
\label{sec:data-layer}

Cominciamo la nostra analisi dell'architettura dal primo livello che si è progettato, quello della base di dati.
Questo livello si occupa dell'elaborazione dei dati, più specificatamente li memorizza, li manipola se necessario, ma soprattutto li recupera efficientemente.

\subsection{Schema concettuale}
\label{subsec:schema-concettuale}

La prima fase della progettazione di questo livello è stata la definizione di uno schema concettuale, come illustrato dal diagramma \textenglish{Entity-Relationship} (ERD) nella \Cref{fig:er}.
%La sintassi del diagramma è semplice e scarna.
%Le entità sono modellate attraverso rettangoli, rappresentano oggetti del nostro d~ominio e possono essere specializzate attraverso una freccia di generalizzazione.
%Ogni entità ha uno o più attributi modellati da un pallino vuoto collegato ad essa e rappresentanti una sua proprietà; qualora il pallino fosse pieno esso risulterebbe un identificatore.
%Le associazioni tra entità sono rappresentate mediante dei rombi, che possono avere attributi.
%Infine un'entità può essere identificata esternamente attraverso una relazione.
%Questa sintassi permette di descrivere il problema graficamente tralasciando la soluzione, il comportamento delle entità o gli aspetti fisici riguardanti la base di dati, ma ponendo attenzione alla normalizzazione.
%Andiamone ad esaminare alcune delle scelte progettuali fatte.

Per modellare il \ref{itm:category} è stato necessario utilizzare un'associazione ricorsiva nella quale si esprime il fatto che una categoria può avere nessuna o più di una sottocategoria.
Inoltre la ricorsività dell'associazione esprime il fatto che una sottocategoria è a sua volta una categoria, che però potrebbe non essere discendete di nessun altra.
Per tali ragioni si è prevista l'opzionalità (cardinalità \texttt{(0,1)}) sul ramo inferiore dell'associazione \texttt{\textenglish{includes}}.

\begin{sidewaysfigure}[p]
	\centering
	\includegraphics[width=\linewidth]{er.pdf}
	\caption{Diagramma ER del progetto.}
	\label{fig:er}
\end{sidewaysfigure}

Osservando l'entità \texttt{\textenglish{product}} si nota la presenza di due attributi \texttt{\textenglish{description}} e\linebreak \verb|description_no_html|, perché sono \textbf{entrambi} necessari?
La ragione deriva dal \ref{itm:search}, infatti la ricerca, compiuta dal \textenglish{database}, non deve prendere in considerazione gli eventuali \textenglish{tag} HTML presenti nella descrizione del prodotto.
Quest'ultima, richiesta dal \ref{itm:product}, è stata modellata come un testo HTML, perché si vuole lasciare all'amministratore la possibilità di descrivere al meglio il prodotto, attraverso un editor WYSIWYG\@.
L'uso di un tale editor è stato fatto anche per inserire la recensione come richiesto dal \ref{itm:review}.

Al prodotto è collegata l'entità \verb|product_image|, la quale descrive una sua particolare ``configurazione''.
Ogni configurazione ha un immagine visibile all'utente (attributo \verb|url|), un testo alternativo all'immagine (\verb|htmlcontent|) e il prezzo (\verb|options_value_price|).
Quest'ultimo è calcolato come incremento rispetto al \texttt{\textenglish{price}} del prodotto, entrambi sono memorizzati come riportati dal listino prezzi, ovvero scorporati dalle tasse e senza sconti.
Si noti inoltre che il prodotto senza alcuna personalizzazione corrisponde alla ``configurazione'' base, per la quale vale \verb|options_value_price = 0|.

Consideriamo ora il \ref{itm:part} e il \ref{itm:part-isa-product}.
Per modellarli è stata usata l'associazione ricorsiva \texttt{\textenglish{comprises}}, per tanto è opportuno specificare i ruoli a cui i due rami dell'associazione si riferiscono.
Un prodotto può avere uno o più ricambi, ma anche nessuno perché potrebbe essere esso stesso un ricambio; tutto ciò è modellato con la cardinalità \texttt{(0,N)}.
Di conseguenza un ricambio sarà usato in almeno un prodotto, fatto che si è modellato usando la cardinalità \texttt{(1,N)}.
Questo discorso può essere riproposto anche per i prodotti che sostituiscono quelli ritirati (\ref{itm:substitutes}), come si vede nell'associazione \texttt{\textenglish{substitutes}}.

L'ultimo requisito che prenderemo in considerazione è il \ref{itm:payment}, il quale viene modellato con le entità \verb|credit_card| e \verb|sale_order| e la loro associazione.
Esaminando \verb|sale_order| si nota che non vi è un attributo per il costo totale dell'ordine, piuttosto si è deciso di separare il costo totale della merce dal costo delle tasse.
Questa scelta è stata fatta perché le tasse imposte possono variare con il tempo e quindi si è preferito scorporarle dal prezzo totale, che può essere ancora ottenuto dalla somma di \verb|subtotal_price| e \verb|tax_price|.
Andando ad esaminare il requisito più nel dettaglio è necessario precisare cosa viene memorizzato qualora il pagamento sia effettuato con una carta di credito oppure PayPal.
Nel primo caso ci sarà \textbf{sempre} un riferimento ai dati della carta di credito, memorizzati separatamente rispetto all'ordine.
Questo significa che qualora vengano fatti più acquisti con la stessa carta di credito da parte di uno stesso cliente, essa sarà memorizzata nuovamente ogni volta.
Tale scelta porterebbe a pensare a uno spreco di memoria, tuttavia questa è la soluzione ideale per non dover gestire le carte di credito di un cliente, ma richiedere solo i dati in fase di pagamento.
Alcuni dei dati che vengono conservati, dopo essere stati processati, sono il \textenglish{\textit{Primary Account Number}} (PAN) troncato, ma generalmente composto da 16 cifre, la data di validità e il titolare; tutti nel rispetto del PCI~DSS~\cite{PSSC2018}, come richiesto dal \ref{itm:pci-dss}.
Tale standard non ammette la memorizzazione, anche cifrata, di dati sensibili di autenticazione, quale per esempio il \textenglish{\textit{Card Validation Code}} o \textenglish{\textit{Value}} (CVC o CVV), usato per respingere transazioni fraudolente.
In particolare la troncatura del PAN può essere fatta mantenendo le prime sei cifre oppure le ultime quattro; al contrario il mantenimento dell'intero PAN deve essere ben documentato e motivato.

Nel caso di PayPal non vi è alcun riferimento alla carta di credito usata.
In entrambi i casi la buona riuscita o meno del pagamento sarà riportata da \verb|settled_at| o \verb|error_msg|, che sono mutualmente esclusivi.

Infine si tiene a precisare che non tutti i requisiti funzionali sono stati qui discussi, perché molti sono immediati dato il diagramma ER\@.
Inoltre si è deciso di introdurre nel diagramma requisiti funzionali non espressi (vedi per esempio entità \texttt{\textenglish{newsletter}}), che però non facendo parte dello sviluppo concordato non sono stati realizzati.
Tale scelta è stata fatta per eventuali espansioni della piattaforma durante la fase di manutenzione.

\subsection{Schema logico e fisico}
\label{subsec:schema-logico-fisico}

Terminato il diagramma ER non si è ancora in grado di realizzare ``fisicamente'' il \textenglish{database}, prima è necessaria una ristrutturazione del diagramma.
Tale operazione è utile per ridurre il livello di astrazione, infatti vi sono costrutti che non possono essere direttamente rappresentati da un \textenglish{database} relazione, n'è un esempio la generalizzazione.

Il diagramma ristrutturato è stato realizzato con l'uso di \prog{\mbox{MySQL} Workbench}, prende il nome di diagramma ER esteso (EERD) ed è mostrato in \Cref{fig:eer}.
Seppur la ristrutturazione poteva mantenere la stessa sintassi usata nel diagramma ER, lo strumento utilizzato propone come linguaggio di modellazione dei dati IDEF1X\@.
Inoltre permette una facile generazione del codice per la definizione della base di dati, motivo per cui nel seguito, durante le osservazioni sul diagramma, se ce ne sarà la necessità, si tratteranno anche alcuni aspetti dello schema fisico.

Ad un primo sguardo del diagramma ristrutturato salta all'occhio che non sono state scelte come chiavi primarie tutti gli identificatori usati nell'ERD, in alcuni casi si sono preferite chiavi surrogate di tipo intero.
Per cui si è deciso di non utilizzare, come chiavi primarie, identificatori stringa o esterni, ma comunque mantenendo i vincoli di \texttt{\textenglish{NOT NULL}} e \texttt{\textenglish{UNIQUE}}.

Riprendendo le associazioni ricorsive si nota che sono state modellate attraverso una chiusura transitiva, come mostrato dalle tabelle \texttt{\textenglish{category}}, \verb|product_has_update| e \linebreak\verb|product_has_part|.
Una chiusura transitiva permette di decomporre una gerarchia, come quella delle categorie e sottocategoria.
Nel caso specifico delle categorie si è associata ad ogni sottocategoria un genitore (vedi \verb|parent_id|), che assume il valore \texttt{NULL} qualora la sottocategoria sia una categoria.
Una chiusura transitiva può essere anche realizzata con una tabella esterna, utile per modellare le associazione \texttt{N:M}.
Pur essendo piuttosto naturali, le chiusure transitive non possono essere attraversate facilmente dal linguaggio SQL/92, che manca di costrutti appropriati ed è quindi non completo.
Tuttavia tale problema non si pone perché, si è fatto uso di un linguaggio di programmazione universale come Java.

Abbiamo già descritto l'entità \verb|product_image|, dicendo che l'\texttt{url} contiene l'indirizzo dell'immagine raffigurante la configurazione del prodotto scelta dal cliente.
Dal diagramma ristrutturato notiamo che la colonna omonima è una stringa di al massimo 2083 caratteri, tale limite è stato scelto basandosi sulla lunghezza gestibile da un vecchio \textenglish{browser} come \prog{Internet Explorer 10}.
In realtà gli RFC non specificano nessun limite particolare, ma è buona norma rimanere sotto i 2000 caratteri.

Tutti gli attributi decimali sono stati modellati con il tipo \verb|DECIMAL(15,4)|, il quale indica che la parte intera è lunga 15 cifre, mentre quella decimale 4.

Si consideri il \ref{itm:privileges}, per modellarlo si è utilizzato l'attributo \verb|is_valid| dell'entità \texttt{\textenglish{login}}, che può assumere i valori \texttt{\textenglish{true}} o \texttt{\textenglish{false}}.
Un cliente che non ha confermato la propria \textenglish{e-mail}, come richiesto dal \ref{itm:sign-up}, avrà \verb|is_valid = false|.
Gli utenti in questa condizione possono essere rimossi da un apposito evento, visto che non avendo completato il profilo non hanno potuto fare alcun acquisto (vedi \ref{itm:profile}), per tanto è indifferente la loro permanenza sulla base di dati.
Si ricorda che un evento è una procedura o funzione memorizzata sullo schema di un \textenglish{database}, che può essere eseguito periodicamente e automaticamente.
Si tiene a precisare che l'eliminazione di un cliente consiste nel slegare le tabelle \texttt{\textenglish{customer}} e \texttt{\textenglish{login}}, ponendo \verb|login_id = NULL| quando un \textenglish{record} della tabella \texttt{\textenglish{login}} viene rimosso, ovvero il vincolo di chiave esterna sull'eliminazione è \texttt{SET NULL}.
Così facendo l'utente non potrà più accedere ai suoi dati, ma l'azienda proprietaria della piattaforma potrà esaminare lo \textbf{storico} delle vendite, compresi gli indirizzi apposti alle bolle di spedizione.

Se si osserva attentamente il diagramma ristrutturato si noterà la presenza della tabella \linebreak\verb|user_role|, ma tale nome non compare all'interno del diagramma ER, perché questa tabella è necessaria al funzionamento di \mbox{\prog{Tomcat}}.
Tuttavia permette di distinguere se un cliente loggato è un'azienda oppure un privato (vedi \ref{itm:user-type}), nel diagramma ER questa distinzione è fatta sulla base della generalizzazione (completa ed esclusiva).

Un altro aspetto importante è la codifica usata dallo schema della base di dati, ovvero \linebreak\verb|utf8mb4_0900_as_cs| (vedi \ref{itm:utf-8}), il quale è basato sulla codifica Unicode 9.0 e gestisce sia gli accenti (\textit{\textenglish{accent sensitive}}) sia la differenza tra maiuscole e minuscole (\textit{\textenglish{case sensitive}}).
Tale codifica è utile nel caso in cui la piattaforma voglia affermarsi anche in un mercato internazionale.

In conclusione si ricorda che sullo schema della base di dati si sono definiti due utenti con privilegi differenti per manipolare i dati.
\begin{sidewaysfigure}[p]
	\centering
	\includegraphics[width=\linewidth]{eer.eps}
	\caption{Diagramma EER del progetto.}
	\label{fig:eer}
\end{sidewaysfigure}
