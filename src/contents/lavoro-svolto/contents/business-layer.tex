% !TeX encoding = UTF-8
% !TeX root = ../../../main.tex

\section{Business layer}
\label{sec:business-layer}

Questo livello occupa il ruolo di collante tra ciò che è mostrato all'utente e i dati che saranno manipolati.
La mole di dati che lo attraversa richiede di essere gestita in maniera efficiente e sicura.
Vista l'importanza di questi aspetti, non gli si poteva non dedicare un paragrafo.

In particolare saranno soprattutto discusse le problematiche di sicurezza che sono sorte, le loro soluzioni e quale impatto sulle prestazioni hanno portato queste ultime.
Infine non mancherà la discussione di un problema di concorrenza, che è sempre presente nelle piattaforme \ecommerce.

Per una discussione più tecnica si veda \Cref{ch:orm}.

\subsection{Il problema della cache}
\label{subsec:il-problema-della-cache}

Le pagine che richiedono un autenticazione non devono poter essere visibili una volta effettuato il \textenglish{logout}.
Tuttavia i \textenglish{browser}, per necessità di efficienza, salvano in \textenglish{cache} le pagine visitate.
Questo comporta la possibilità di recuperare dalla cronologia, le pagine, che richiedono autenticazione nonostante si sia fatto il \textenglish{logout}.
Per impedire questo comportamento, il \textenglish{browser} non dovrebbe memorizzare le pagine sensibili in \textenglish{cache}.
Ciò può essere realizzato impostando opportunamente gli \textenglish{header} HTTP di tali pagine.
Più precisamente, prima che la risposta giunga al \textenglish{client} vi è un filtro (vedi \Cref{ch:strumenti-utilizzati}) che inserisce nell'\textenglish{header} i seguenti valori:
\begin{verbatim}
	Cache-Control: no-cache, no-store, must-revalidate
	Pragma: no-cache
\end{verbatim}
A questo punto qualora una pagina, che necessita di autenticazione, venga nuovamente richiesta, non essendo trovata nella \textenglish{cache}, sarà inviata dal \textenglish{server}, che potrà fare gli opportuni controlli.
Per tanto pagando un piccolo prezzo in termini di prestazioni, si ha una maggiore sicurezza.

\subsection{Accounting}
\label{subsec:accounting}

In questo \namecref{subsec:accounting} esamineremo il \textenglish{login} e la registrazione.
Partiamo da quest'ultima.

La registrazione consiste nel completamento di un \textenglish{form} richiedente un'\textenglish{e-mail} e una \textenglish{passphrase} di almeno 8 caratteri.
Così facendo si suggerisce all'utente di usare una frase piuttosto che una parola come \textenglish{password}, per facilitarne il ricordo.

Prima di poter inviare la richiesta di registrazione il cliente è tenuto ha completare il reCAPTCHA proposto, la cui risposta sarà allegata alla richiesta.
In particolare si è usato il reCAPTCHA versione 2, il quale propone una sfida all'utente ovvero selezionare le immagine che contengono oppure no un determinato oggetto.
È d'obbligo specificare la versione usata, perché Google ha sviluppato ben tre versioni differenti.
Nella prima, oramai deprecata, viene richiesto all'utente di inserire il testo che legge, esattamente come l'originale sistema CAPTCHA\@.
Al contrario nell'ultima versione non viene proposta sfida all'utente, anzi gli viene assegnato un punteggio stabilito in base all'interazione con il sito \textenglish{web}.
L'utilità del reCAPTCHA è di evitare registrazioni fatte da \textenglish{bot}, realizzabili, per esempio, con librerie e strumenti messi a disposizione dal progetto Selenium~\cite{selenium}.
Inoltre si prevengono attacchi \textenglish{distributed denial of service} (DDoS).

Verificato lato server la risposta del reCAPTCHA, si passa al controllo dell'\textenglish{e-mail}, sfruttando i \textenglish{Mail eXchanger (MX) record}.
In particolare si scorpora il dominio dall'\textenglish{e-mail}, usando come separatore il carattere ``\verb|@|'', si ottiene la lista degli MX \textenglish{record}, che risolvo quel dominio, e si verifica che non sia vuota.
Qualora tale lista fosse vuota si restituisce un errore all'utente.
Si noti che tale controllo non assicura l'esistenza della casella di posta sul dato dominio, pertanto è possibile creare \account con \textenglish{e-mail} basata su domini reali, ma caselle di posta inesistenti.
Tuttavia tale problema può essere ovviato controllando che l'\textenglish{e-mail} di conferma dell'\account arrivi a destinazione.
In caso contrario è possibile eliminare l'\account appena creato.

Qualora tutti i controlli passassero si può procedere con l'effettiva memorizzazione dell'\account sulla base di dati.
Quest'operazione implica il salvataggio della \textenglish{passphrase} in maniera sicura.
In primo luogo bisogna considerare che l'utente generalmente riutilizza le stesse \textenglish{password}, per tale motivo è necessario prevedere un meccanismo che non renda visibile la \textenglish{password} né agli utenti legittimi, come ai manutentori della base di dati, né a un potenziale malintenzionato, che ottiene illecitamente l'accesso.
Per fare ciò si utilizza una funzione di derivazione della chiave, che presa in ingresso una \textenglish{passphrase} restituisce una chiave crittografica.
Si è scelto come algoritmo PBKDF2~\cite{Moriarty2017}, il quale, oltre alla \textenglish{passphrase}, prende in input una stringa casuale (il \textenglish{salt}), il numero di iterazioni, la lunghezza in bit della chiave derivata e la funzione pseudocasuale da applicare, nel nostro caso HMAC\@.
Tale meccanismo impedisce sia un attacco a forza bruta sia a dizionario, in effetti tali attacchi risultano più difficili all'aumentare del numero di iterazioni.
Oltre a questi attacchi, il \textenglish{salt} garantisce l'impossibilità di precalcolare le chiavi corrispondenti alle password più probabili (\textenglish{rainbow attack}) e non necessita di essere mantenuto segreto.
Infatti sul \textenglish{database} le credenziali saranno memorizzate nella forma \verb|salt$iterationCount$encodedCredential|.

Effettuata la registrazione l'utente può fare il \textenglish{login}.
Il \textenglish{form} che mette a disposizione questa funzionalità richiede un'\textenglish{e-mail} e una \textenglish{password}, come da \ref{itm:login}.
Dall'\textenglish{e-mail} si ricercano le credenziali crittografate, che vengono usate per validare la \textenglish{password} fornita.
Qualora l'\textenglish{e-mail} non corrisponda a nessun \account, viene comunque derivata una chiave dalla password fornita, prima di restituire l'errore.
Questo non è lavoro inutile, ma previene i \textenglish{timing attack}, ovvero l'attaccante non può dedurre alcuna informazione utile basandosi sul tempo di risposta della richiesta.
Inoltre effettuando il \textenglish{login} vi è un \textenglish{refresh} del valore del \textenglish{cookie} di sessione (\textenglish{\texttt{JSESSIONID}}), in tal modo si prevengono \textenglish{session fixation attack}.

Infine si vuol far presente che qualora l'accesso venga sbagliato per 5 volte consecutive, non sarà permesso accedere per 5 minuti.
Anche questa misura è stata adottata per evitare attacchi a forza bruta, infatti ogni 5 tentativi vi è un lasso di tempo che rallenta sensibilmente il processo di attacco.

\subsection{XSS}
\label{subsec:xss}

Abbiamo già detto che la descrizione di un prodotto e il corpo di una recensione sono editabili attraverso un editor WYSIWYG (vedi \Cref{subsec:schema-concettuale}), il cui principale vantaggio è quello di essere percepito, da parte dell'utente, come un qualunque \textenglish{word processor}, nascondendo l'HTML e il CSS di cui fa uso.
Per gli utenti più esperti è comodo lavorare direttamente con questi linguaggi.

Così facendo però, è possibile inserire all'interno dell'HTML, codice JavaScript, che verrebbe salvato sul \textenglish{database} e mandato in esecuzione ogni volta che la pagina, che lo contiene, fosse recuperata.
L'editor diventa un veicolo per sferrare un attacco \textenglish{cross-site scripting} (XSS) da parte di malintenzionati, perciò bisogna evitarlo.
La soluzione adotta è stata quella di ripulire (\textenglish{\textit{sanitize}}), con la libreria AntiSamy~\cite{antisamy}, i testi contenenti codice HTML, da potenziale codice JavaScript.

\subsection{IDOR}
\label{subsec:idor}

Abbiamo mostrato l'uso intensivo delle chiavi surrogate (vedi \Cref{fig:eer}), che permettono di individuare l'oggetto di interesse.
Viene naturale pensare di esporre tali chiavi agli utenti della piattaforma, per esempio inserendole all'interno dell'URL\@.
Tale approccio è chiamato \textenglish{Insecure Direct Object Reference} (IDOR).
Pur non costituendo un problema di sicurezza diretta, l'attaccante potrebbe stabilire il \textenglish{pattern} su cui una chiave primaria si basa e sferrare un \textenglish{enumeration attack}, il cui scopo sarebbe ottenere l'accesso ai dati con quel formato di chiave.

Per proteggersi da questo attacco, piuttosto che mostrare le chiavi in chiaro, basterebbe esporre una loro trasformazione difficilmente reversibile (\textenglish{one-way}).
Per questa ragione ogni chiave esposta prima è stata concatenata con un \textenglish{pepper}, ovvero un \textenglish{salt} fisso, e successivamente passata a una funzione SHA-256.
Si è scelta tale funzione, piuttosto che SHA-512, perché si voleva identificatori brevi, ma robusti.

Tuttavia la soluzione proposta necessita di calcolare ogni volta, per ogni richiesta, la funzione \textenglish{hash}, dato che i valori non cambiano, si è usata una struttura dati per conservare l'associazione tra chiave e identificatore calcolato.
Tale struttura non ammette duplicati ed è condivisa tra tutte le richieste gestite da una data servlet (vedi \Cref{ch:strumenti-utilizzati}), in maniera tale che tutti ne possano beneficiare senza allocare ulteriore spazio.
In particolare la struttura dati scelta è una \texttt{BiMap} implementata da Guava~\cite{guava}.

\subsection{Concorrenza}
\label{subsec:concorrenza}

Cosa succede se due utenti acquistano contemporaneamente lo stesso prodotto, ma la quantità totale richiesta è superiore alle scorte in magazzino?
Questo sarà il problema affrontato nel \namecref{subsec:concorrenza}.

Il comportamento desiderato sarebbe quello in cui uno solo dei due utenti effettua l'ordine, mentre l'altro viene rimandato alla pagina del carrello per essere avvisato della quantità insufficiente.
Questo comportamento può essere ottenuto usando le transazioni.
Una transazione è un lotto atomico di operazioni, nel quale o tutte le operazioni sono portate a termine oppure non lo è nessuna (\textenglish{rollback}).
Prima di creare effettivamente l'ordine, una transazione pone un \textenglish{lock} sui prodotti della tabella \textenglish{\texttt{product}} che si vogliono acquistare, al fine di poter eseguire le \textenglish{query} per decrementarne le quantità.
Se durante tale operazione dovesse giungere un nuovo ordine, in cui tutti i prodotti sono in quantità sufficiente, ma che comprende, anche solo parzialmente, gli stessi articoli dell'ordine in processamento, questo sarebbe trattenuto fino al completamento dell'operazione precedente.
Una volta che l'ordine è in attesa di pagamento (per maggiori dettagli si veda \Cref{ch:strumenti-utilizzati}), quello nuovo può essere processato, tuttavia è possibile che un prodotto non sia più in quantità sufficiente, perché già venduto nell'ordine precedente.
Ricordando che esistono controlli ad ogni livello (vedi \Cref{sec:architettura}), all'atto del decremento della quantità si avrà la violazione del vincolo sulla base di dati per cui \verb|quantity >= 0|.
Questo fatto terminerebbe il processamento della transazione e l'utente sarebbe reindirizzato al carrello, da cui gli spetterà scegliere quale azione intraprendere.

Anche se le quantità fossero sufficienti è ancora possibile, che la transazione non sia completata.
Un ordine prima di essere creato deve essere pagato, quindi il lato server riceve i dati del metodo di pagamento, li processa e se nessun errore viene sollevato finalizza la transazione (\textenglish{commit}) scrivendo gli aggiornamenti delle quantità.
Un semplice meccanismo, ma che presenta un difetto, infatti durante il processamento della transazione (monetaria) il \textenglish{lock} sulle righe della tabella \textenglish{\texttt{product}} è ancora presente, per tanto nessun altro ordine, che abbia, anche solo parzialmente, gli stessi articoli, può essere processato.
Tale fatto si potrebbe ripercuotere sulle prestazioni, ecco perché si è cercato di ridurre la quantità di computazione tra il lancio della transazione e la sua finalizzazione.
Ad esempio si è dichiarato ed eventualmente inizializzato le variabili necessarie prima di lanciare la transazione, in modo tale che queste operazioni non fossero eseguite quando il \textenglish{lock} era attivo, garantendone però la presenza qualora vi si faccia riferimento.

Qualora non si usassero le transazioni, le \textenglish{query} verrebbero eseguite non appena possibile, ma tale comportamento porterebbe ad uno stato di inconsistenza e nessun ordine sarebbe realmente evaso.
