userdata = userdata or {}

--- Add leading zeros to a number.
--- @param digits string: number of digits
--- @param number string: a number to be zero padded
--- @return string: a number zero padded
function userdata:zeropadding(digits, number)
    assert(tonumber(digits),
            "invalid input: " .. digits .. " is not a number")
    assert(tonumber(number),
            "invalid input: " .. number .. " is not a number")
    return string.format("%0" .. digits .. "d", number)
end

return userdata