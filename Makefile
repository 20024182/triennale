# Executables
latexmk = latexmk

# Main file name
MAIN = FERRON_relazione
MAIN_READABLE = $(MAIN)-readable
MAIN_PRINTABLE = $(MAIN)-printable
MAIN_ABSTRACT = $(MAIN)-abstract
MASTER = main
MASTER_TEX = $(MASTER).tex
WORKING_DIR = src/

all: readable printable abstract

printable:
	cd $(WORKING_DIR) && \
	rm -vf $(MASTER).pdf && \
	$(latexmk) -interaction=nonstopmode -shell-escape -synctex=1 -lualatex -usepretex='\newif\ifprint \printtrue\newif\ifabstract \abstracttrue' $(MASTER_TEX) && \
	cp $(MASTER).pdf ../out/$(MAIN_PRINTABLE).pdf

readable:
	cd $(WORKING_DIR) && \
	rm -vf $(MASTER).pdf && \
	$(latexmk) -interaction=nonstopmode -shell-escape -synctex=1 -lualatex -usepretex='\newif\ifprint \printfalse\newif\ifabstract \abstracttrue' $(MASTER_TEX) && \
	cp $(MASTER).pdf ../out/$(MAIN_READABLE).pdf

abstract:
	cd $(WORKING_DIR) && \
	rm -vf $(MASTER).pdf && \
	$(latexmk) -interaction=nonstopmode -shell-escape -synctex=1 -lualatex -usepretex='\newif\ifprint \printfalse\newif\ifabstract \abstractfalse' $(MASTER_TEX) && \
	cp $(MASTER).pdf ../out/$(MAIN_ABSTRACT).pdf
